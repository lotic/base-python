--------------------
Building and Testing
--------------------
Clone the repository::

  git clone https://bitbucket.org/lotic/base-python && cd base-python
  virtualenv -p python3 .venv && source .venv/bin/activate
  pip3 install -r requirements.txt

Run the sample program::

  python -m lotic_app.contrib

In a separate window::

  python -m tests.util send_test_message

-------------
Documentation
-------------
Python documentation is available at http://lotic-python-base.readthedocs.io/, but has some issues related to *Read The Docs* not yet supporting Python 3.5+, see https://github.com/rtfd/readthedocs.org/issues/1990 for more info.
