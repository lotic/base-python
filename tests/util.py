import argparse
import boto3
import os.path

from lotic_app import load_env

__STACK_NAME__ = 'base-test-stack'


def load_file_as_string(rel_path):
    abs_path = os.path.abspath(os.path.join(os.path.dirname(__file__), rel_path))
    with open(abs_path) as f:
        return f.read()


def create_test_stack():
    """Creates the test stack that has both an SNS Topic and SQS Queue."""
    cloudformation = boto3.client('cloudformation')
    stack_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'stack.yml'))
    with open(stack_path) as f:
        template = f.read()
    response = cloudformation.create_stack(
        StackName=__STACK_NAME__,
        TemplateBody=template,
    )
    print(response)


def create_env_file():
    """Creates the test stack that has both an SNS Topic and SQS Queue."""
    cloudformation = boto3.resource('cloudformation')
    outputs = cloudformation.Stack(__STACK_NAME__).outputs
    outputs = {o['OutputKey']: o['OutputValue'] for o in outputs}
    envpath = os.path.abspath(os.path.join(os.path.dirname(os.path.dirname(__file__)), '.env'))
    with open(envpath, 'w') as f:
        f.write('SQS_QUEUE_URL={}\n'.format(outputs['QueueUrl']))
        f.write('SNS_TOPIC_ARN={}\n'.format(outputs['TopicArn']))
    with open(envpath) as f:
        print(f.read())


def send_test_message():
    """Sends a test message to the AWS SNS Topic."""
    load_env()
    sns = boto3.resource('sns')
    topic = sns.Topic(os.environ['SNS_TOPIC_ARN'])
    request = load_file_as_string('data/apprequest.json')
    result = topic.publish(Message=request)
    print(result)


def _main():
    public_vars = [globals().get(var) for var in globals() if not var[0] is '_']
    commands = {var.__name__: var for var in public_vars if callable(var)}
    parser = argparse.ArgumentParser(description='Integration App Provisioning')
    parser.add_argument('command', metavar='COMMAND', help='command to run', choices=commands.keys())
    args = parser.parse_args()
    command = commands[args.command]
    command()

if __name__ == '__main__':
    _main()
