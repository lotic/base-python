import unittest
import json
import collections
from lotic_app.app import Attachment, DataSource, LoticApp, Request, RequestHandler

from .util import load_file_as_string

__TEST_ATTACHMENT__ = load_file_as_string('data/attachment.json')
__TEST_DATASOURCE__ = load_file_as_string('data/datasource.json')
__TEST_APPREQUEST__ = load_file_as_string('data/apprequest.json')


class TestRequestParsing(unittest.TestCase):

    def __assert_attachment(self, attachment):
        self.assertIsNotNone(attachment, 'Attachment.parse returned None')
        self.assertIsInstance(attachment, Attachment, 'attachment is not instance of Attachment class')
        self.assertEqual(attachment.type, Attachment.UNSTRUCTURED_URL)
        self.assertEqual(attachment.value, "https://www.example.com")

    def test_parse_attachment_from_string(self):
        attachment = Attachment.parse(__TEST_ATTACHMENT__)
        self.__assert_attachment(attachment)

    def test_parse_attachment_from_dict(self):
        attachment = json.loads(__TEST_ATTACHMENT__)
        attachment = Attachment.parse(attachment)
        self.__assert_attachment(attachment)

    def test_parse_attachment_list(self):
        def assert_list():
            self.assertIsNotNone(attachment, 'Attachment.parse returned None')
            self.assertIsInstance(attachment, collections.Iterable, 'attachment is not instance of list')
            self.assertEqual(len(list(attachment)), 2, 'attachments list is wrong size')
            for item in attachment:
                self.assertEqual(item.type, Attachment.UNSTRUCTURED_URL)
                self.assertEqual(item.value, "https://www.example.com")
        # Create a list of two strings
        attachment = [__TEST_ATTACHMENT__, __TEST_ATTACHMENT__]
        attachment = Attachment.parse(attachment)
        assert_list()
        # Create a list of one string, and one parsed value
        attachment = [json.loads(__TEST_ATTACHMENT__), __TEST_ATTACHMENT__]
        attachment = Attachment.parse(attachment)
        assert_list()

    def __assert_data_source(self, data_source):
        self.assertIsNotNone(data_source, 'DataSource.parse returned None')
        self.assertIsInstance(data_source, DataSource, 'data_source is not instance of DataSource class')
        self.assertEqual(data_source.id, '1234')
        self.assertEqual(data_source.name, "Test Data Source")
        self.assertIsInstance(data_source.attachments, collections.Iterable)
        attachments = list(data_source.attachments)
        self.assertEqual(1, len(attachments))
        self.__assert_attachment(attachments[0])

    def test_parse_data_source_from_string(self):
        data_source = DataSource.parse(__TEST_DATASOURCE__)
        self.__assert_data_source(data_source)

    def test_parse_request_from_string(self):
        request = Request.parse(__TEST_APPREQUEST__)
        self.assertIsNotNone(request, 'Request.parse returned None')
        self.assertIsInstance(request, Request, 'request is not instance of Request class')
        self.assertEqual(request.id, '1234')
        self.assertEqual(request.type, "my_message")
        self.assertEqual(request.response_url, "https://s3.amazonaws.com/app-events.lotic.net/test")
        self.assertIsInstance(request.data_sources, collections.Iterable)
        data_sources = list(request.data_sources)
        self.assertEqual(1, len(data_sources))
        self.__assert_data_source(data_sources[0])


class TestRequestHandler(unittest.TestCase):

    def test_requires_valid_name(self):
        app = LoticApp()
        with self.assertRaises(TypeError):
            app.msg_handler(None)
        with self.assertRaises(TypeError):
            app.msg_handler(1)
        with self.assertRaises(ValueError):
            app.msg_handler('')
        with self.assertRaises(ValueError):
            app.msg_handler('invalid string')
        with self.assertRaises(ValueError):
            app.msg_handler('invalid-string')
        app.msg_handler('valid_message_type')
        app.msg_handler('net.lotic.events.sync_attachments')

    def test_call_handler(self):

        def handler_func(_):
            return {'key': 'value'}

        request = Request.parse(__TEST_APPREQUEST__)
        handler = RequestHandler(handler_func)

        with self.assertRaises(TypeError):
            handler(None)

        with self.assertRaises(TypeError):
            handler(1)

        with self.assertRaises(TypeError):
            handler(dict(key=1, value=1))

        self.assertEqual(handler(request), {'key': 'value'})


class TestRequestDispatch(unittest.TestCase):

    def __assert_lookup(self, app, value):
        request = Request.parse(__TEST_APPREQUEST__)
        handler = app.get_handler(request)
        self.assertIsNotNone(handler)
        self.assertTrue(callable(handler))
        result = handler(request)
        self.assertEqual(value, result)

    def test_handler_lookup_by_name_param(self):
        app = LoticApp()

        @app.msg_handler('my_message')
        def my_message_handler(_):
            return {'key': 'value'}

        self.__assert_lookup(app, {'key': 'value'})


if __name__ == '__main__':
    unittest.main()
