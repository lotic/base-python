# TODO: Add validators to these variables, i.e. proper URL or ARN format
__REQUIRED_ENV_VARS__ = ('DB_HOST', 'DB_NAME', 'DB_USER', 'DB_PASS', 'DB_SCHEMA', 'SNS_TOPIC_ARN', 'SQS_QUEUE_URL')
"""Required environment variables."""


def bootstrap():
    """Configure the environment and logging in one step."""
    from lotic_app.logging import conf_logging
    from lotic_app import load_env
    load_env()
    conf_logging()


def load_env():
    """
    Loads environment variables using `dotenv`_, if a ``.env`` file can be located. If no such file is found, it will
    log a warning, but proceed anyway, assuming the required environment variables are properly set.

    Below is a summary of the required environment variables validated by this function.

    ================= =================================================================================
    Name              Description
    ================= =================================================================================
    **SQS_QUEUE_URL** The URL of the `AWS SQS Queue`_ used to retrieve messages for this integration.
    **SNS_TOPIC_ARN** The identifier of the `AWS SNS Topic`_ used to post messages to this integration.
    **DB_HOST**       The integration's database host name.
    **DB_NAME**       The integration's database name.
    **DB_SCHEMA**     The integration's database schema, see `PostgreSQL Schema`_ for more.
    **DB_USER**       The integration's database user name.
    **DB_PASS**       The integration's database password.
    ================= =================================================================================

    There is no **DB_PORT** value, as the database currently uses `5432` as the default PostgreSQL port.

    .. _sample project: https://bitbucket.org/lotic/sample-integration
    .. _dotenv: http://example.com/
    .. _Data Docs: https://datadocs.loticlabs.com
    .. _PostgreSQL Schema: https://www.postgresql.org/docs/current/static/ddl-schemas.html
    .. _AWS SNS Topic: http://docs.aws.amazon.com/sns/latest/dg/welcome.html
    .. _AWS SQS Queue: http://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/Welcome.html
    """
    import os
    import sys
    import logging
    from dotenv import load_dotenv, find_dotenv

    try:
        logger = logging.getLogger(__name__)
        dot_env_file = find_dotenv(raise_error_if_not_found=True)
        load_dotenv(dot_env_file)
    except IOError as e:
        logger.warning('.env file not found, using environment variables: %s', str(e))
    except Exception as e:
        logger.error('unable to load .env file: %s', str(e))
        sys.exit(1)

    # Validate environment variables
    missing_vars = [var_name for var_name in __REQUIRED_ENV_VARS__ if not os.environ.get(var_name)]
    if len(missing_vars):
        raise ValueError('missing required environment variables: ' + ', '.join(missing_vars))
