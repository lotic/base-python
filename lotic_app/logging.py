"""
=======
Logging
=======
Logs are writen in JSON format so that they can be easily archived and searched in AWS.
"""
import sys
import logging
import logging.config


def conf_logging():
    """Configure logging to use JSON logger."""
    __LOG_CONFIG__ = {
        'version': 1,
        'handlers': {
            'console': {
                'class': 'logging.StreamHandler',
                'level': 'INFO',
                'formatter': 'json',
                'stream': sys.stdout,
            },
        },
        'formatters': {
            'json': {
                'class': 'pythonjsonlogger.jsonlogger.JsonFormatter',
            },
        },
        'loggers': {

            '': {
                'level': 'INFO',
                'handlers': ['console'],
                'propagate': False,
            },
            'bottle': {
                'level': 'INFO',
                'handlers': ['console'],
                'propagate': False,
            },
        }
    }
    logging.config.dictConfig(__LOG_CONFIG__)
