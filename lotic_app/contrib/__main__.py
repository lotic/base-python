from lotic_app.logging import conf_logging
from lotic_app import load_env
conf_logging()

from lotic_app.app import LoticApp

load_env()
app = LoticApp()


@app.msg_handler('ping')
def handle_ping(request):
    # request is a lotic_app.Request object
    return {'message': 'PONG!'}

app.run()
