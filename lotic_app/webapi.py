"""
=======
Web API
=======
The :py:mod:`lotic_app.app` module contains the core classes used to create a Lotic Labs data integration app. The main
class is :py:class:`LoticApp`, and it manages the message queueing and response round-trip mechanisms. The other classes
in this module deal mainly with the request/response data and reading in the requests from `JSON`.
"""
import bottle
import logging
from functools import wraps
from threading import Thread

__REQUEST_LOG_KEYS__ = ('remote_addr', 'method', 'url')

logger = logging.getLogger(__name__)


class WebAPI(Thread):
    """Runs the Bottle web API and health check."""

    def __init__(self, **kwargs):
        """Initializes a new web API thread."""
        super().__init__(name='WebAPI', daemon=True)
        self.__bottle_args = kwargs
        self.__bottle = bottle.Bottle()
        self.__bottle.install(self._log_to_logger)
        self.__bottle.get('/')(lambda: {'status': 'ok'})  # health check

    @staticmethod
    def _log_to_logger(fn):
        """Wrap a bottle response in a JSON log entry."""

        @wraps(fn)
        def decorator(*args, **kwargs):
            actual_response = fn(*args, **kwargs)
            info = {key: getattr(bottle.request, key) for key in __REQUEST_LOG_KEYS__}
            info['status'] = bottle.response.status_code
            logger.info('HTTP request', extra=info)
            return actual_response

        return decorator

    def run(self):
        """Run the web API as a Bottle app."""
        logger.info('starting web service on port 5050')
        self.__bottle.run(**self.__bottle_args)
