"""
=================
Framework Classes
=================
The :py:mod:`lotic_app.app` module contains the core classes used to create a Lotic Labs data integration app. The main
class is :py:class:`LoticApp`, and it manages the message queueing and response round-trip mechanisms. The other classes
in this module deal mainly with the request/response data and reading in the requests from `JSON`.
"""
import os
import sys
import boto3
import json
import traceback
import collections
import re
import requests
from retrying import retry
from logging import getLogger

__MESSAGE_TYPE_PATTERN__ = re.compile("^[\w.]+$")

logger = getLogger(__name__)


def _json_parse(cls, value, from_dict=None):
    """Parse an object from: string, dict or list."""
    if isinstance(value, cls):
        return value
    if isinstance(value, str):
        return _json_parse(cls, json.loads(value), from_dict)
    if isinstance(value, collections.Mapping):
        if callable(from_dict):
            return from_dict(value)
        return cls(**value)
    if isinstance(value, collections.Iterable):
        return [_json_parse(cls, item, from_dict) for item in value]


def _get_formatted_exception():
    """Format an exception and traceback into something suitable for serializing to JSON."""
    info = sys.exc_info()
    return traceback.format_exception(*info)


class Attachment:
    """
    An attachment is an item owned by a :py:class:`DataSource` that identifies a direct endpoint through which data can
    be accessed. Generally, this is a URL relating to an HTTP resource. Other URI schemes are allowed, however, such as
    FTP. In addition to the :py:attr:`~value` of the attachment (i.e. the URL), there is a :py:attr:`~type` that gives
    a hint as to how this attachment should be interpreted.
    """

    STRUCTURED_FILE = 'SF'
    """Structured File"""

    UNSTRUCTURED_FILE = 'UF'
    """Unstructured File"""

    STRUCTURED_URL = 'SU'
    """Structured URL"""

    UNSTRUCTURED_URL = 'UU'
    """Unstructured URL"""

    API = 'WS'
    """API or Web Service"""

    MANIFEST = 'M'
    """Download URL Manifest"""

    OTHER = 'O'
    """Other"""

    __VALID_TYPES__ = (STRUCTURED_FILE, UNSTRUCTURED_FILE, STRUCTURED_URL, UNSTRUCTURED_URL, API, MANIFEST, OTHER)
    """Valid attachment types."""

    type = property(lambda self: self.__type)
    """Get the type of the attachment."""

    value = property(lambda self: self.__value)
    """Get the value of the attachment."""

    dict = property(lambda self: dict(type=self.type, value=self.value))
    """Returns a dictionary representing this object, with proper key names."""

    def __init__(self, type_=None, value=None):
        """Initializes a new attachment reference using the provided type and value."""

        # Validate type
        if not isinstance(type_, str):
            raise TypeError('type_ must be a string')
        type_ = type_.strip()
        if not type_ or type_ not in self.__VALID_TYPES__:
            raise ValueError('invalid attachment type: ' + type_)

        # Validate value
        if not isinstance(value, str):
            raise TypeError('value must be a string')
        value = value.strip()
        if not value:
            raise ValueError('value must not be empty')

        self.__type = type_
        self.__value = value

    @classmethod
    def parse(cls, value):
        """Parse a value into an attachment or list of attachments."""
        def from_dict(the_dict):
            the_dict['type_'] = the_dict.pop('type')  # reserved keyword
            return Attachment(**the_dict)

        return _json_parse(Attachment, value, from_dict)

    def __str__(self):
        """Return a JSON string representation of this object."""
        return json.dumps(self.dict)

    def __repr__(self):
        """Return a JSON string representation of this object."""
        return self.__str__()


class DataSource:
    """
    A data source is the container object of an :py:class:`Attachment`. Data sources have zero or more attachments, so
    that URLs or files on the web can be grouped into logically similar groups of information. For example, each
    attachment might refer to a different year within the same type of annual report provided by a government agency.
    Logically, this is the same :py:class:`DataSource`, but the location of the data are different (different URLs). To
    group all of these similar files together, we use the data source abstraction.
    """

    id = property(lambda self: self.__id)
    """The ID of the data source."""

    name = property(lambda self: self.__name)
    """The name of the data source."""

    dict = property(lambda self: dict(id=self.id, name=self.name, attachments=[atc.dict for atc in self.attachments]))
    """Returns a dictionary representing this object, with proper key names."""

    def __init__(self, id_, name, attachments=[]):
        """Initializes a new data source reference using the provided type and value."""
        self.__id = str(id_)
        self.__name = str(name)
        self.__attachments = list(attachments)

    @property
    def attachments(self):
        """Gets an iterable of :py:class:`Attachment` items."""
        for attachment in self.__attachments:
            yield attachment

    @classmethod
    def parse(cls, value):
        """Parse a value into a data source or list of data sources."""
        def from_dict(the_dict):
            attachments = the_dict.get('attachments')
            attachments = Attachment.parse(attachments) if attachments else []
            if not isinstance(attachments, collections.Iterable):
                attachments = [attachments]
            the_dict['id_'] = the_dict.pop('id')  # reserved keyword
            the_dict['attachments'] = attachments
            return DataSource(**the_dict)

        return _json_parse(Attachment, value, from_dict)

    def __str__(self):
        """Return a JSON string representation of this object."""
        return json.dumps(self.dict)


class Request:
    """
    A request represents the client-side (integration) reference to an integration event created by `Data Docs`_ to
    perform some named action on a data source and its set of attachments. In other words, `Data Docs`_ creates the
    event in its database and then uses that information to generate a :py:class:`Request` for the integration to
    process. This request contains a :py:attr:`~response_url` attribute that is a pre-signed Amazon S3 URL, allowing
    the integration to `PUT` a JSON document to that URL within 24 hours.
    """

    id = property(lambda self: self.__request_id)
    """The unique ID of the request."""

    type = property(lambda self: self.__request_type)
    """The type of the request."""

    response_url = property(lambda self: self.__response_url)
    """The AWS S3 pre-signed URL to which the integration responds after processing a request."""

    dict = property(lambda self: dict(id=self.id, type=self.type, response_url=self.response_url,
        data_sources=[ds.dict for ds in self.data_sources]))
    """Returns a dictionary representing this object, with proper key names."""

    def __init__(self, id_, type_, response_url, data_sources=[]):
        """Initialize a new request from the incoming JSON value."""
        self.__request_id = id_
        self.__request_type = type_
        self.__response_url = response_url
        self.__data_sources = data_sources or []

    @property
    def data_sources(self):
        """Gets an iterable of :py:class:`DataSource` items."""
        for data_source in self.__data_sources:
            yield data_source

    @classmethod
    def parse(cls, value):
        """Parse a value into a request or list of requests."""
        def from_dict(the_dict):
            data_sources = the_dict.get('data_sources')
            data_sources = DataSource.parse(data_sources) if data_sources else []
            if not isinstance(data_sources, collections.Iterable):
                data_sources = list[data_sources]
            the_dict['id_'] = the_dict.pop('id')      # reserved keyword
            the_dict['type_'] = the_dict.pop('type')  # reserved keyword
            the_dict['data_sources'] = data_sources
            return Request(**the_dict)

        return _json_parse(Attachment, value, from_dict)

    @classmethod
    def parse_from(cls, file_path):
        with open(file_path) as f:
            return cls.parse(f.read())

    def __str__(self):
        """Return a JSON string representation of this object."""
        return json.dumps(self.dict)


class Response:
    """Encapsulates an application response."""

    request = property(lambda self: self.__request)
    """The :py:class:`Request` to which this is a response."""

    success = property(lambda self: not self.__exception)
    """Indicates if the request was successfully processed and if there was an exception thrown."""

    value = property(lambda self: self.__value)
    """A JSON document containing the integration-provided response value."""

    exception = property(lambda self: self.__value)
    """An exception (if present), describing the error that caused :py:attr:`~success` to be ``False``."""

    dict = property(lambda self: self.__response)
    """Returns a dictionary representing this object, with proper key names."""

    def __init__(self, request, value=None, exception=None):
        """Initializes a new response using the provided request, and either value or exception, but not both."""

        # Ensure proper arguments
        if not isinstance(request, Request):
            raise TypeError('request must be of type Request')
        if not value and not exception:
            raise ValueError('value and exception cannot both be None')

        self.__request = request
        self.__value = value
        self.__exception = exception

        # Create a dict representation of the response
        self.__response = {'status': 'S' if self.success else 'F'}
        if self.value:
            self.__response['value'] = self.value
        if self.exception:
            self.__response['exception'] = self.exception

    def __str__(self):
        """Return a JSON string representation of this object."""
        return json.dumps(self.dict)

    @retry(stop_max_attempt_number=3)
    def send(self):
        """Responds to a request by issuing an HTTP PUT to a pre-signed S3 URL."""

        # Common logging args
        log_args = {
            'request_id': self.request.id,
            'request_type': self.request.type,
            'response_url': self.request.response_url,
            'success': self.success,
        }

        try:
            logger.info('responding to request_url', extra=log_args)
            response = requests.put(self.request.response_url, json=self.__response)
            response.raise_for_status()
            log_args['response'] = {'status': response.status_code}
            if response.text:
                log_args['response']['text'] = response.text
            logger.info('request response complete', extra=log_args)
        except requests.RequestException:
            e = _get_formatted_exception()
            logger.error('unable to send response to response_url', extra={**log_args, 'exception': e})


class RequestHandler:
    """
    Wrapper for a request handler callable.
    """

    __str__ = property(lambda self: self.__callable.__str__)
    __repr__ = property(lambda self: self.__callable.__repr__)

    def __init__(self, callable_):
        """Create a new request handler containing a reference to the underlying callable."""

        # Ensure that we can call this function later
        if not callable(callable_):
            raise ValueError('must be a callable')

        self.__callable = callable_

    def __call__(self, request):
        """Wrapped handler invocation."""

        # Make sure the request is properly formed
        if not isinstance(request, Request):
            raise TypeError('request must be a valid Request object')

        # Common logging attributes
        log_args = {
            'handler': self.__repr__,
            'request': {
                'id': request.id,
                'type': request.type,
            }
        }

        try:
            logger.info('invoking handler function', extra=log_args)
            return self.__callable(request)
        except Exception as e:
            info = sys.exc_info()
            logger.error('error invoking handler function', extra={**log_args,
                                                                   'exception': traceback.format_exception(*info),
                                                                   })
            raise LoticApp.HandlerExecutionError(e)


class LoticApp:
    """Base Lotic Application framework."""

    __REQUEST_CLASS_NAME__ = Request.__class__.__module__ + '.' + Request.__class__.__name__

    class HandlerNotFoundError(BaseException):
        """Handler not found for message type."""
        pass

    class HandlerExecutionError(BaseException):
        """Handler encountered error."""
        pass

    def __init__(self, **kwargs):
        """Create an AWS SQS message poller."""
        super().__init__()
        self.opts = {'WaitTimeSeconds': 20, 'MessageAttributeNames': ['All'], **kwargs}
        self.__msg_handlers = {}

    def __process_message(self, sqs_message):
        """Process an individual message."""
        try:
            # Get inner message from SQS wrapper
            sqs_message_body = json.loads(sqs_message.body)
            sns_message = sqs_message_body.get('Message')
            if not sns_message:
                raise ValueError('"Message" attribute not found in sqs_message_body')
            # Parse the request into a Lotic app request object
            app_request = Request.parse(sns_message)
            self.dispatch(app_request)
        except:
            e = _get_formatted_exception()
            logger.error('unable to process message', extra={'exception': e})
        finally:
            sqs_message.delete()

    def get_handler(self, request):
        """Get the request handler for the given request."""
        handler = self.__msg_handlers.get(request.type)
        if handler:
            return handler
        raise LoticApp.HandlerNotFoundError(request)

    def dispatch(self, request):
        """Dispatch a request to a message handler."""

        # Make sure we receive a request argument
        if not isinstance(request, Request):
            raise ValueError('request must be an instance of ' + self.__REQUEST_CLASS_NAME__)

        # Common log args
        log_args = {'request_id': request.id, 'request_type': request.type}

        # Get request handler
        try:
            logger.info('looking up request handler', extra=log_args)
            handler = self.get_handler(request)
            logger.info('found request handler', extra={**log_args, 'handler': handler.__repr__})
        except LoticApp.HandlerNotFoundError:
            e = _get_formatted_exception()
            logger.info('request handler not found', extra=log_args)
            # Fail the operation, returning a failed result
            response = Response(request, exception=e)
            response.send()
            return

        # Invoke the request handler
        try:
            handler_result = handler(request)
            response = Response(request, value=handler_result)
        except LoticApp.HandlerExecutionError:
            e = _get_formatted_exception()
            logger.error('Unable to invoke handler', extra={**log_args, 'exception': e})
            response = Response(request, exception=e)

        # Finally, respond with the result of the handler
        response.send()

    def msg_handler(self, message_type):
        """Decorator to define a message request handler."""

        # Ensure message_type is a string
        if not isinstance(message_type, str):
            raise TypeError('message_type must be a string')

        # Ensure message_type is proper format
        if not __MESSAGE_TYPE_PATTERN__.match(message_type):
            raise ValueError('message_type must match pattern ' + __MESSAGE_TYPE_PATTERN__.pattern)

        # Check that message key is valid
        handler_key = str(message_type)

        def decorator(f):
            if not callable(f):
                raise TypeError('msg_handler decorator can only be applied to callables')
            log_args = {
                'message_type': handler_key,
                'callable': repr(f),
            }
            logger.info('creating request handler', extra=log_args)
            wrapped = RequestHandler(f)
            log_args['handler'] = repr(wrapped)
            logger.info('registering request handler', extra=log_args)
            self.__msg_handlers[handler_key] = wrapped
            logger.debug('registered message handler', extra=log_args)
            return wrapped

        return decorator

    def __run_poller(self):
        """Receives custom resource requests from message queue and performs provisioning."""

        # Get SQS_QUEUE_URL from environment
        queue_url = os.environ.get('SQS_QUEUE_URL')
        if not queue_url:
            logger.error('SQS_QUEUE_URL not found, shutting down')
            return

        # Create queue
        queue = boto3.resource('sqs').Queue(os.environ['SQS_QUEUE_URL'])
        logger.info('polling for messages', extra={'queue_url': queue.url})

        # @retry(stop_max_attempt_number=3)
        def receive_messages():
            """Wrap the message receiving in a retry handler."""
            return queue.receive_messages(**self.opts)

        # TODO: Make shutdown of this loop more graceful
        while True:
            try:
                sqs_messages = receive_messages()
                for sqs_message in sqs_messages:
                    self.__process_message(sqs_message)
            except KeyboardInterrupt:
                return

    @staticmethod
    def __run_websvc():
        """Runs the Flask web API and health check."""
        try:
            from .webapi import WebAPI
            WebAPI(host='0.0.0.0', port=5050, quiet=True).start()
        except ImportError as e:
            logger.error('{} is not installed, unable to import WebAPI'.format(e.name))
            if e.name is not 'bottle':
                raise

    def run(self):
        """Run the application poller."""
        handlers = {key: value.__repr__() for (key, value) in self.__msg_handlers.items()}
        logger.info('beginning thread pool', extra={'handlers': handlers})

        # Launch background thread and poller
        self.__run_websvc()
        self.__run_poller()
