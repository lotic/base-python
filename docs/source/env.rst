Environment Variables
=====================

.. autofunction:: lotic_app.load_env

When your integration app is deployed and running in AWS, it will be provided with these environment variables
automatically. You do not need to check in the ``.env`` file into source control. In fact, the `sample project`_ has
added the ``.env`` file to ``.gitignore`` for this reason.

---------------------
Downloading .env File
---------------------
The easiest way to set all of these variables is to download the ``.env`` file from the `Resources` tab in the `Data Docs`_
integration detail page and save it in the root folder of your application.

.. figure:: images/download_dotenv.png
   :scale: 50 %
   :alt: Download .env file for integration

   You can download the `.env` file from your `Data Docs`_ integration detail page by clicking the link, as shown
   in the diagram.

.. _sample project: https://bitbucket.org/lotic/sample-integration
.. _Data Docs: https://datadocs.loticlabs.com
