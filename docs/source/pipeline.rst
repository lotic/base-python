Request Processing Pipeline
===========================

Requests are processed using a basic pipeline:

#. `Data Docs`_ creates an integration :py:class:`Request` and posts that request to the integration's `AWS SNS Topic`_.
#. This framework polls for messages on its `AWS SQS Queue`_, passing received requests to the integration.
#. The integration inspects the :py:class:`Request`, looking at the :py:class:`Attachment` items to be analyzed.
#. The integration invokes coordinates its own activities to analyze the data, and generates a `JSON` response.
#. This framework then completes this request with a `PUT` request of the data back to :py:attr:`Request.response_url`.
#. `Data Docs`_ is notified that the request is complete, and updates the status of the event in the database.

.. _sample project: https://bitbucket.org/lotic/sample-integration
.. _dotenv: http://example.com/
.. _Data Docs: https://datadocs.loticlabs.com
.. _AWS SNS Topic: http://docs.aws.amazon.com/sns/latest/dg/welcome.html
.. _AWS SQS Queue: http://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/Welcome.html
