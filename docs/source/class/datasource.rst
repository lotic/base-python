DataSource
==========

.. module:: lotic_app.app
.. autoclass:: DataSource
  :members:

  .. literalinclude :: ../../../tests/data/datasource.json
     :language: json
