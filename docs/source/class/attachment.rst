Attachment
==========

.. module:: lotic_app.app
.. autoclass:: Attachment
  :members:

  .. literalinclude :: ../../../tests/data/attachment.json
     :language: json
