Response
========

.. module:: lotic_app.app
.. autoclass:: Response
  :members:

  .. literalinclude :: ../../tests/data/apprequest.json
     :language: json
