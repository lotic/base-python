Request
=======

.. module:: lotic_app.app
.. autoclass:: Request
  :members:

  .. literalinclude :: ../../../tests/data/apprequest.json
     :language: json
