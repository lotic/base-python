Lotic Python App Base Docs
==========================

The `Lotic Python App Base` is a Python 3.6 base library that performs some basic plumbing to connect to an
`AWS SQS Queue`_, receive integration requests, and asynchronously respond to those requests. Responses are received by
the hosting environment by monitoring HTTP ``PUT`` requests to the URL provided to the request. The developer is free to
choose how to handle these requests, and what data to provide in the response. The response may be send any time within
24 hours of receiving the request. After that point, :py:attr:`Request.response_url` is no longer valid for the request.

To get started on building an integration app, you should carefully read through this documentation and take a look at
the `sample project`_ on Bitbucket. This library is intentionally compact and unopinionated so that its key features
can be replicated in other environments or programming languages.

.. _sample project: https://bitbucket.org/lotic/sample-integration
.. _AWS SQS Queue: http://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/Welcome.html

.. toctree::
   :maxdepth: 1
   :caption: Getting Started:

   pipeline
   env
   sample

.. toctree::
  :maxdepth: 1
  :caption: Framework Classes
  :name: classes
  :glob:

  class/*

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
